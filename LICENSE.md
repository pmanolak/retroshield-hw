# RetroShield for Arduino Mega

8BitForce.com uses two different licenses for design files — one for hardware and one for software.

Hardware:
---------

**8BitForce.com hardware is released under [Creative Commons Share-alike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/).**

Feel free to use, copy, modify, distribute for any purpose, even commercially.

Please give credit to 8BitForce.com and share any derivative work under the same license.

Distributed as-is. 
No warranties are given. 

Software:
---------

**8-Bit-Force code, firmware, and software is released under the MIT License(http://opensource.org/licenses/MIT).**

The MIT License (MIT)

Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Third Party:
------------

Credits/Licenses for third party products/code used and/or modified:

Hardware/makefile 
Hardware/renamepcb.py: 
Blake Bourque 
http://www.techwizworld.net/ 
[TechplexEngineer/Makefile](https://gist.github.com/TechplexEngineer/af6115adaa44567a1723)
