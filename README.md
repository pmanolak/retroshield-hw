
![picture](https://gitlab.com/8bitforce/retroshield-hw/raw/master/docs/images/software_breadboarding2.jpg)

## RetroShield for Arduino Mega

I'm excited to announce a very cool project for retromaniacs:

* A real 8 bit microprocessor connected to Arduino or Teensy
* Build your 8-bit microprocessor project as simple as writing Arduino Code.
* Make system hardware changes quickly by modifying Arduino code.
* Works with Arduino Mega (cheap, easy) or Teensy 3.5/3.6 (faster, more memory).
* Open source hardware and software.
* Easy to build kits available for sale.

---

### Completed RetroShields:

My goal is to build a daughtercard for every microprocessor out there. This is what I have completed so far.
  
| Microprocessor | Emulated Hardware | Software |
|:--------------:|:-----------------:|----------|
| **Intel 4004**            | printer + kbd       | Busicom 141-PF Calculator ([www.4004.com](http://www.4004.com)) |
| **Intel 8031**            | 8031 + UART         | PAULMON2 (Paul Stoffregen) |
| **Intel 8085**            | 8085A + 8251        | MON85 (David Dunfield) |
| **MOS 6502**              | Apple-1 (6502+6521)<br>KIM-1 (6502 + 6530) | MON,BASIC,Cassette (Steve W)<br>KIM-1 (MOS Technology, Inc) |
| **Motorola 6803**         | 6803 + 6850         | Bill Beech's 6800 Monitor Code |
| **Motorola 6809**         | 6809E + FTDI        | Simon6809 (Lennart Benschop's monitor modifed by Erturk)|
| **RCA 1802**              | 1802 + UART         | MCSMP (Charles J, Yakym)<br>Tiny BASIC (Tom Pittman)<br>RCA BASIC3 (Ron Cenker) |
| **Signetics 2650**        | 2650 + UART         | Signetics PipBug M20 Monitor Code |
| **Zilog Z80**             | Z80 + 8251          | Microsoft Basic (MSFT, Grant Searle)<br>EFEX (Mustafa Peker) |
| **SC/MP II**              | INS8060 + UART      | Kitbug and NIBL|
| **HD6120 PDP-8**          | HD-6120 + UART + OS8 | HD-6120 running Robert Armstrong's [SBC6120 HW](http://www.sparetimegizmos.com/Hardware/SBC6120-2.htm)|

---

## Arduino Mega 2560 Features:
  *  <img src="./docs/images/RetroShield6502_in_action.jpg" alt="Retroshield in action" width="500"/>
  * ROM : >200 KB
  * RAM : ~6 KB (2KB left over for Arduino system sw)
  * Microprocessor runs at about 100~200kHz.
  * Peripherals: UART, PIA, Timers, etc. (emulated by Arduino)
  * Arduino Shields can be used to add new features.
## Teensy 3.5/3.6 Features:
  * <img src="./docs/images/teensy2retroshield.jpg" alt="Retroshield in action" width="500"/>
  * Microprocessor runs at 1.00+ Mhz.
  * RAM : 256 KB
  * ROM : >512 KB
  * Provides 3.3V<->5V level shifting
  * microSD slot (can be used to emulate disk drives)
  * Peripherals: UART, PIA, Timers, etc. (emulated by Teensy)

---

### Design Files

All hardware and software is open source.

* [RetroShield Hardware](https://gitlab.com/8bitforce/retroshield-hw)
* [RetroShield Arduino Code](https://gitlab.com/8bitforce/retroshield-arduino)
* [RetroShield Teensy Code](https://gitlab.com/8bitforce/retroshield-teensy)
* [Wiki Page](https://gitlab.com/8bitforce/retroshield-hw/-/wikis/home)

---

### Upcoming microprocessors:

I'm currently working on the bold highlighted processors.

  * Intel: 8008, 4040, 8080, 8088
  * Motorola: 14500B (1-bit ICU), 6800, 68008
  * Signetics: 8X300
  * Fairchild: F8
  * Texas Instruments TMS9900
  * Commodore SID
  * EEPROM Read/Write

---

### Kit Sales

* Kits are available on [8BitForce's Store](https://www.tindie.com/stores/8bitforce/)

---

### License Information

* Copyright 2019 Erturk Kocalar, 8Bitforce.com
* Hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
* Software is released under the [MIT License](http://opensource.org/licenses/MIT).
* Distributed as-is; no warranty is given.
* See LICENSE.md for details.

---

### Pictures

**2024/02/28:**<br>
RetroShield 6120 (PDP-8) is released with Teensy code that emulates SBC6120 HW.

<img src="./docs/images/RetroShield4004_withcpu.jpg" alt="RetroShield4004" width="400"/>
<img src="./docs/images/k6803_kit.jpg" alt="RetroShield6803" width="400"/>
<img src="./docs/images/k2650_kit.jpg" alt="RetroShield2650" width="400"/>
<img src="./docs/images/k6502_kit.jpg" alt="RetroShield6502" width="400"/>
<img src="./docs/images/k6809_kit.jpg" alt="RetroShield6809e" width="400"/>
<img src="./docs/images/kZ80_kit2.jpg" alt="RetroShieldZ80" width="400"/>
<img src="./docs/images/k1802_kit.jpg" alt="RetroShield1802" width="400"/>
<img src="./docs/images/k8031_kit.jpg" alt="RetroShield8031" width="400"/>
<img src="./docs/images/k8085_kit.jpg" alt="RetroShield8085" width="400"/>
<img src="./docs/images/k8060_kit_CPU.jpg" alt="RetroShield SC/MP II" width="400"/>





