# RetroShield for Arduino Mega

Design files released to production.

#### Release process

Once a design is released to manufacturing:
1) Copy the working folder to this folder with revision + date.
2) Copy the gerber files sent to fab house.
3) Create pdf versions of sch, pcb copper and pcb silk for doc/ folder.

*Note: I come from hardware background, so you will see hardware specific terms as opposed to software.*